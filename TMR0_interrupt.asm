; Assembler code for a pic 18f2420
; Intention is to use timer interrupts
; to blink an LED
INCLUDE	"p18f2420.inc"

  CONFIG	OSC = INTIO67	; use the internal oscillator with pins 6 and 7 for general purpose I/O

  org	0
  goto main
  org	0x008
  goto	interrupt_serv
  org 0x0200

main
;;; Set the RA0 pin high, RA<1:7> low
  clrf	PORTA	; clear all of the output data latches
  movlw	0x00
  movwf	TRISA	; set RA<0:7> as output pins
  movlw	0x07
  movwf	PORTA	; set RA0 high
;;; Done with initial setting of RA pins

;;; Set the internal oscillator to 8MHz
  bsf	OSCCON,	IRCF2
  bcf	OSCCON,	IRCF1
  bcf	OSCCON,	IRCF0

;;; Configure the Timer0 Module to use the internal clock
;;; with a 1:256 prescaler and enable interrupts
  bsf	INTCON, TMR0IE	; enable Timer0 interrupts
  bcf	INTCON, TMR0IF	; clear the interrupt flag
  bcf	RCON, IPEN	; disable interrupt priorities
  bsf	INTCON, GIE	; enable all interrupts
  movlw 0x88
  movwf T0CON

loop
  ;nop
  goto	loop	; infinite loop

interrupt_serv	; we get here by TMR0 overflowing
  
  btg	LATA, 2
  bcf	INTCON,	TMR0IF	; clear the interrupt flag
  retfie

  end
